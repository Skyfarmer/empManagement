#include "claimlockerdialog.hpp"
#include "ui_claimlockerdialog.h"

claimLockerDialog::claimLockerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::claimLockerDialog)
{
    ui->setupUi(this);
}

void claimLockerDialog::show(libemployee::Locker::SEX sex)
{

    libemployee::Database db(QSqlDatabase::database());
    QString condition;
    QList<libemployee::Employee> tempEmp;
    QList<int> tempInt;

    condition.append("WHERE isMasc = ");
    if(sex == libemployee::Locker::MALE)
        condition.append("1");
    else
        condition.append("0");

    ui->comboLocker->clear();
    ui->comboLocker->setDisabled(false);
    currentLockers.clear();

    // Let's check if lockers are occupied or not
    db.fetchEmployee(&tempEmp);
    for(int i = 0; i < tempEmp.count(); i++)
        tempInt.append(tempEmp.at(i).getLockerID());

    db.fetchLocker(&currentLockers, condition);
    for(int i = 0; i < currentLockers.count(); i++)
    {
        if(tempInt.contains(currentLockers.at(i).getintID()))
        {
            currentLockers.removeAt(i);
            i--;
            continue;
        }
        QString temp;
        temp.append(tr("Room: ") + QString::number(currentLockers.at(i).getRoom()));
        temp.append(tr("/Number: ") + QString::number(currentLockers.at(i).getNum()));
        ui->comboLocker->addItem(temp);
    }

    if(currentLockers.isEmpty())
    {
        emit reject();
        QApplication::beep();
        return;
    }

    QDialog::show();
}

void claimLockerDialog::accept()
{
    emit accepted(currentLockers.at(ui->comboLocker->currentIndex()).getintID());
}

claimLockerDialog::~claimLockerDialog()
{
    delete ui;
}
