#include "csqlquerymodel.hpp"

CSqlQueryModel::CSqlQueryModel(QList<libemployee::Locker> &lockdb, QObject *parent) : QSqlQueryModel(parent)
{
    this->lockdb = lockdb;
}

QVariant CSqlQueryModel::data(const QModelIndex &item, int role) const
{
    int column = item.column();
    QString value;

    switch(column)
    {
        case 1:
        if(!(QSqlQueryModel::data(item).toString().isEmpty()))
        {
            for(int i = 0; i < lockdb.count(); i++)
            {
                if(lockdb.at(i).getintID() == QSqlQueryModel::data(item).toInt())
                {
                    value.append(QString::number(lockdb.at(i).getRoom()));
                    value.append(" / ");
                    value.append(QString::number(lockdb.at(i).getNum()));
                    break;
                }
            }
        }
        break;
        case 2:
            int isMasc = QSqlQueryModel::data(item).toInt();
            if(isMasc == 1)
                value.append(tr("Man"));
            else
                value.append(tr("Woman"));
        break;
    }
    if(value.isEmpty())
        return QSqlQueryModel::data(item, role);
    else if(role == Qt::DisplayRole)
        return value;
    else
        return QVariant();
}
