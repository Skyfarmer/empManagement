#ifndef EMPVIEW_HPP
#define EMPVIEW_HPP

#include <QMainWindow>
#include <QHeaderView>
#include <QSqlQueryModel>
#include <QSortFilterProxyModel>
#include <QComboBox>
#include <QLineEdit>
#include <QUndoStack>

#include "addemployeedialog.hpp"
#include "claimlockerdialog.hpp"
#include "csqlquerymodel.hpp"
#include "commands.hpp"
#include "dependencies.hpp"

namespace Ui {
class empView;
}

class empView : public QMainWindow
{
    Q_OBJECT

public:
    explicit empView(QString dbpath, QWidget *parent = 0);
    ~empView();

protected:
    void contextMenuEvent(QContextMenuEvent *event);

private:
    Ui::empView *ui;
    addEmployeeDialog *addDialog;
    addEmployeeDialog *updateDialog;
    claimLockerDialog *claimDialog;

    QLineEdit *lineSearch;
    QComboBox *comboSearchFilter;

    CSqlQueryModel *empModel;
    QSortFilterProxyModel *sortModel;
    libemployee::Database *mainDB;

    QUndoStack *mainUndoStack;
    QAction *actionUndo;
    QAction *actionRedo;

    void restoreAppState();
    void storeAppState();
    void storeSettings();

private slots:
    void addEmployee(libemployee::Employee emp);
    void updateEmployee(libemployee::Employee emp);
    void removeEmployee();
    void releaseLocker();
    void claimLocker(int lockID);
    void showUpdateDialog();
    void showClaimLockerDialog();
    void refreshTable();
    void refreshTable(int index);
    void changeFilter(QString text);
    void changeFilter(int column);
    void showAboutDialog();

signals:
    void added();
    void updated();
    void removed();
    void released();
    void claimed();
};

#endif // EMPVIEW_HPP
