#include "employee.hpp"
using namespace libemployee;

// *************GET FUNCTIONS***************
int Employee::getID() const
{
    return this->userId;
}

int Employee::getLockerID() const
{
    return this->lockerId;
}

Employee::SEX Employee::getSex() const
{
    return this->sex;
}

QString Employee::getTitle() const
{
    return this->title;
}

QString Employee::getFirstName() const
{
    return this->firstName;
}

QString Employee::getLastName() const
{
    return this->lastName;
}

QString Employee::getDepartment() const
{
    return this->department;
}

//************SET FUNCTIONS***************
void Employee::setID(int id)
{
    this->userId = id;
}

void Employee::setLockerID(int lockerId)
{
    this->lockerId = lockerId;
}

void Employee::setSex(SEX sex)
{
    this->sex = sex;
}

void Employee::setTitle(QString title)
{
    this->title = title;
}

void Employee::setFirstName(QString firstName)
{
    this->firstName = firstName;
}

void Employee::setLastName(QString lastName)
{
    this->lastName = lastName;
}

void Employee::setDepartment(QString department)
{
    this->department = department;
}
