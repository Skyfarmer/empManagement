#include "locker.hpp"
using namespace libemployee;

Locker::Locker(int intID, int num, int room, SEX sex)
{
    this->intID = intID;
    this->num = num;
    this->room = room;
    this->sex = sex;
}

int Locker::getintID() const
{
    return this->intID;
}

int Locker::getNum() const
{
    return this->num;
}

int Locker::getRoom() const
{
    return this->room;
}

Locker::SEX Locker::getSex() const
{
    return this->sex;
}
