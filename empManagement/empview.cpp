#include "empview.hpp"
#include "ui_empview.h"

#include <QAction>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlQuery>
#include <QRegExp>
#include <QContextMenuEvent>
#include <QSettings>
#include <QStandardPaths>
#include <QFileInfo>
#include <QDebug>

empView::empView(QString dbpath, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::empView)
{
    ui->setupUi(this);

    // Setting up a few extra things
    // The main window
    this->setWindowTitle(tr("Employee Database"));
    // Connect to the database
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbpath);
    db.open();
    // First of all, our awesome custom dialogs
    addDialog = new addEmployeeDialog(this);
    addDialog->setWindowFlags(Qt::Sheet);
    addDialog->setModal(true);
    updateDialog = new addEmployeeDialog(this);
    updateDialog->setWindowFlags(Qt::Sheet);
    updateDialog->setModal(true);
    claimDialog = new claimLockerDialog(this);
    claimDialog->setWindowFlags(Qt::Sheet);
    claimDialog->setModal(true);

    // Set up the header
    QList<libemployee::Locker> tempLock;
    mainDB = new libemployee::Database(db);
    mainDB->fetchLocker(&tempLock);
    empModel = new CSqlQueryModel(tempLock, ui->empTableView);
    empModel->setQuery("SELECT * FROM employees", QSqlDatabase::database());
    empModel->setHeaderData(0, Qt::Horizontal, tr("User ID"));
    empModel->setHeaderData(1, Qt::Horizontal, tr("Room / Locker"));
    empModel->setHeaderData(2, Qt::Horizontal, tr("Gender"));
    empModel->setHeaderData(3, Qt::Horizontal, tr("Title"));
    empModel->setHeaderData(4, Qt::Horizontal, tr("First Name"));
    empModel->setHeaderData(5, Qt::Horizontal, tr("Last Name"));
    empModel->setHeaderData(6, Qt::Horizontal, tr("Department"));
    sortModel = new QSortFilterProxyModel;
    sortModel->setSourceModel(empModel);
    ui->empTableView->setModel(sortModel);
    ui->empTableView->resizeColumnsToContents();
    ui->empTableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->empTableView->setSortingEnabled(true);

    //Setting up the undo stack
    mainUndoStack = new QUndoStack(this);
    actionUndo = mainUndoStack->createUndoAction(this, tr("Undo: "));
    actionRedo = mainUndoStack->createRedoAction(this, tr("Redo: "));
    actionUndo->setShortcut(QKeySequence::Undo);
    actionRedo->setShortcut(QKeySequence::Redo);

    // Add some more action
    ui->actionAdd_Employee->setShortcut(QKeySequence::New);
    ui->actionUpdate_Employee->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_U));
    ui->actionRemove_Employee->setShortcut(QKeySequence::Delete);
    ui->actionRelease_Locker->setShortcut(QKeySequence::Replace);
    ui->actionClaim_Locker->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_G));
    ui->actionAdd_Employee->setIcon(QIcon(":/icons/add-employee.png"));
    ui->actionAdd_Employee->setIconVisibleInMenu(false);
    ui->actionClaim_Locker->setIcon(QIcon(":icons/claim-locker.png"));
    ui->actionClaim_Locker->setIconVisibleInMenu(false);
    ui->actionRelease_Locker->setIcon(QIcon(":/icons/release-locker.png"));
    ui->actionRelease_Locker->setIconVisibleInMenu(false);
    ui->actionRemove_Employee->setIcon(QIcon(":/icons/remove-employee.png"));
    ui->actionRemove_Employee->setIconVisibleInMenu(false);
    ui->actionUpdate_Employee->setIcon(QIcon(":/icons/update-employee.png"));
    ui->actionUpdate_Employee->setIconVisibleInMenu(false);

    ui->mainToolBar->addAction(ui->actionAdd_Employee);
    ui->mainToolBar->addAction(ui->actionUpdate_Employee);
    ui->mainToolBar->addAction(ui->actionRemove_Employee);
    ui->mainToolBar->addSeparator();
    ui->mainToolBar->addAction(ui->actionRelease_Locker);
    ui->mainToolBar->addAction(ui->actionClaim_Locker);

    lineSearch = new QLineEdit(this);
    lineSearch->setPlaceholderText(tr("Search"));
    lineSearch->setFixedWidth(200);
    lineSearch->setClearButtonEnabled(true);
    comboSearchFilter = new QComboBox(this);
    comboSearchFilter->addItem(tr("by ID"));
    comboSearchFilter->addItem(tr("by first name"));
    comboSearchFilter->addItem(tr("by last name"));
    comboSearchFilter->addItem(tr("by department"));
    comboSearchFilter->adjustSize();
    ui->mainToolBar->addSeparator();
    ui->mainToolBar->addWidget(lineSearch);
    ui->mainToolBar->addWidget(comboSearchFilter);

    QMenu *editMenu = new QMenu(tr("Edit"), this);
    editMenu->addAction(actionUndo);
    editMenu->addAction(actionRedo);
    menuBar()->insertMenu(ui->menuEmployee->menuAction(), editMenu);

    // Read the settings
    restoreAppState();

    connect(ui->actionAdd_Employee, SIGNAL(triggered()), addDialog, SLOT(show()));
    connect(ui->actionUpdate_Employee, SIGNAL(triggered()), this, SLOT(showUpdateDialog()));
    connect(ui->actionRemove_Employee, SIGNAL(triggered()), this, SLOT(removeEmployee()));
    connect(ui->actionRelease_Locker, SIGNAL(triggered()), this, SLOT(releaseLocker()));
    connect(ui->actionClaim_Locker, SIGNAL(triggered()), this, SLOT(showClaimLockerDialog()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(showAboutDialog()));
    connect(addDialog, SIGNAL(accepted(libemployee::Employee)), this, SLOT(addEmployee(libemployee::Employee)));
    connect(updateDialog, SIGNAL(accepted(libemployee::Employee)), this, SLOT(updateEmployee(libemployee::Employee)));
    connect(claimDialog, SIGNAL(accepted(int)), this, SLOT(claimLocker(int)));
    connect(lineSearch, SIGNAL(textEdited(QString)), this, SLOT(changeFilter(QString)));
    connect(comboSearchFilter, SIGNAL(currentIndexChanged(int)), this, SLOT(changeFilter(int)));
    connect(this, SIGNAL(added()), addDialog, SLOT(close()));
    connect(this, SIGNAL(added()), this, SLOT(refreshTable()));
    connect(this, SIGNAL(updated()), updateDialog, SLOT(close()));
    connect(this, SIGNAL(updated()), this, SLOT(refreshTable()));
    connect(this, SIGNAL(removed()), this, SLOT(refreshTable()));
    connect(this, SIGNAL(released()), this, SLOT(refreshTable()));
    connect(this, SIGNAL(claimed()), this, SLOT(refreshTable()));
    connect(this, SIGNAL(claimed()), claimDialog, SLOT(close()));
    connect(mainUndoStack, SIGNAL(indexChanged(int)), this, SLOT(refreshTable(int)));
}

void empView::addEmployee(libemployee::Employee emp)
{
    addCommand *cmd = new addCommand(&emp);
    mainUndoStack->push(cmd);
    emit added();
}

void empView::updateEmployee(libemployee::Employee emp)
{
    QList<libemployee::Employee> tempEmp;
    mainDB->fetchEmployee(&tempEmp, "WHERE userID = " + QString::number(emp.getID()));

    updateCommand *cmd = new updateCommand(&tempEmp.first(), &emp);
    mainUndoStack->push(cmd);

    emit updated();
}

void empView::removeEmployee()
{
    QItemSelectionModel *model = ui->empTableView->selectionModel();
    QList<libemployee::Employee> tempEmp;
    int row;
    int id;
    if(!(model->hasSelection()))
        return;

    row = model->selectedIndexes().first().row();
    id = sortModel->data(sortModel->index(row, 0), Qt::DisplayRole).toInt();
    mainDB->fetchEmployee(&tempEmp, "WHERE userID = " + QString::number(id));

    QString message;
    message.append(tr("You are about to remove "));
    message.append(tempEmp.first().getFirstName() + " ");
    message.append(tempEmp.first().getLastName());
    message.append(tr(" with the user ID ") + QString::number(tempEmp.first().getID()));
    message.append(tr(".", "REMOVAL"));

    int result;
    result = QMessageBox::warning(this, tr("Removing employee"), message, QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
    if(result == QMessageBox::Cancel)
        return;

    deleteCommand *cmd = new deleteCommand(&tempEmp.first());
    mainUndoStack->push(cmd);

    emit removed();
}

void empView::releaseLocker()
{
    QItemSelectionModel *model = ui->empTableView->selectionModel();
    QList<libemployee::Employee> tempEmp;
    int row;
    int id;
    if(!(model->hasSelection()))
        return;

    row = model->selectedIndexes().first().row();
    id = sortModel->data(sortModel->index(row, 0), Qt::DisplayRole).toInt();
    mainDB->fetchEmployee(&tempEmp, "WHERE userID = " + QString::number(id));

    if(tempEmp.first().getLockerID() == -1)
        return;

    QString message;
    message.append(tr("You are about to release a locker from "));
    message.append(tempEmp.first().getFirstName() + " ");
    message.append(tempEmp.first().getLastName());
    message.append(tr(" with the user ID ") + QString::number(tempEmp.first().getID()));
    message.append(tr("."));

    int result;
    result = QMessageBox::warning(this, tr("Releasing locker"), message, QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
    if(result == QMessageBox::Cancel)
        return;

    releaseCommand *cmd = new releaseCommand(&tempEmp.first());
    mainUndoStack->push(cmd);

    emit released();
}

void empView::claimLocker(int lockID)
{
    QItemSelectionModel *model = ui->empTableView->selectionModel();
    QList<libemployee::Employee> tempEmp;
    int row;
    int id;

    row = model->selectedIndexes().first().row();
    id = sortModel->data(sortModel->index(row, 0), Qt::DisplayRole).toInt();
    mainDB->fetchEmployee(&tempEmp, "WHERE userID = " + QString::number(id));

    tempEmp.first().setLockerID(lockID);

    QString message;
    message.append(tr("You are about to claim a locker for "));
    message.append(tempEmp.first().getFirstName() + " ");
    message.append(tempEmp.first().getLastName());
    message.append(tr(" with the user ID ") + QString::number(tempEmp.first().getID()));
    message.append(tr(".", "CLAIMING"));

    int result;
    result = QMessageBox::warning(claimDialog, tr("Claiming locker"), message, QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
    if(result == QMessageBox::Cancel)
        return;

    claimCommand *cmd = new claimCommand(&tempEmp.first(), lockID);
    mainUndoStack->push(cmd);

    emit claimed();
}


void empView::showUpdateDialog()
{
    QItemSelectionModel *model = ui->empTableView->selectionModel();
    QList<libemployee::Employee> tempEmp;
    int row;
    int id;
    if(!(model->hasSelection()))
        return;

    row = model->selectedIndexes().first().row();
    id = sortModel->data(sortModel->index(row, 0), Qt::DisplayRole).toInt();
    mainDB->fetchEmployee(&tempEmp, "WHERE userID = " + QString::number(id));

    updateDialog->show(tempEmp.first());
}

void empView::showClaimLockerDialog()
{
    // Check whether user already has a locker
    QItemSelectionModel *model = ui->empTableView->selectionModel();
    QList<libemployee::Employee> tempEmp;
    libemployee::Locker::SEX sex;
    int row;
    int id;
    if(!(model->hasSelection()))
        return;

    row = model->selectedIndexes().first().row();
    id = sortModel->data(sortModel->index(row, 0), Qt::DisplayRole).toInt();
    mainDB->fetchEmployee(&tempEmp, "WHERE userID = " + QString::number(id));

    if(tempEmp.first().getLockerID() != -1)
    {
        int result;
        result = QMessageBox::information(this, tr("Employee already has locker"), tr("The selected employee already has a locker. We can automatically release it."), QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
        if(result == QMessageBox::Cancel)
            return;
    }
    if(tempEmp.first().getSex() == libemployee::Employee::MALE)
        sex = libemployee::Locker::MALE;
    else
        sex = libemployee::Locker::FEMALE;

    claimDialog->show(sex);
}

void empView::refreshTable()
{
    QItemSelectionModel *model = ui->empTableView->selectionModel();
    QByteArray headerState = ui->empTableView->horizontalHeader()->saveState();
    int row, column;
    bool isSelected = !model->selectedIndexes().isEmpty();
    QString query;
    if(isSelected)
    {
        row = model->selectedIndexes().first().row();
        column = model->selectedIndexes().first().column();
    }
    query = empModel->query().lastQuery();
    empModel->setQuery("", QSqlDatabase::database());
    empModel->setQuery(query, QSqlDatabase::database());
    ui->empTableView->horizontalHeader()->restoreState(headerState);
    if(isSelected)
        model->select(sortModel->index(row, column), QItemSelectionModel::Select);
}

void empView::refreshTable(int index)
{
    refreshTable();
}

void empView::changeFilter(QString text)
{
    if(text.isEmpty())
    {
        sortModel->setFilterRegExp("");
        return;
    }
    QRegExp exp;

    text.insert(0, "^");

    exp.setPattern(text);
    exp.setCaseSensitivity(Qt::CaseInsensitive);
    sortModel->setFilterRegExp(exp);
}

void empView::changeFilter(int column)
{
    switch(column)
    {
    case 0:
        sortModel->setFilterKeyColumn(0);
        break;
    case 1:
        sortModel->setFilterKeyColumn(4);
        break;
    case 2:
        sortModel->setFilterKeyColumn(5);
        break;
    case 3:
        sortModel->setFilterKeyColumn(6);
        break;
    }
    changeFilter(lineSearch->text());
}

void empView::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.addAction(ui->actionAdd_Employee);
    menu.addAction(ui->actionUpdate_Employee);
    menu.addAction(ui->actionRemove_Employee);
    menu.addSeparator();
    menu.addAction(ui->actionRelease_Locker);
    menu.addAction(ui->actionClaim_Locker);

    if(ui->empTableView->horizontalHeader()->underMouse() || ui->empTableView->verticalHeader()->underMouse())
        return;

    menu.exec(event->globalPos());
}

void empView::restoreAppState()
{
    QSettings setting;
    setting.beginGroup("WindowProperties");
    this->resize(setting.value("size", QSize(900, 600)).toSize());
    this->move(setting.value("pos", QPoint(200, 200)).toPoint());
    this->restoreState(setting.value("state").toByteArray());
    ui->empTableView->horizontalHeader()->restoreState(setting.value("headerState").toByteArray());
    setting.endGroup();
}

void empView::storeAppState()
{
    QSettings setting;
    setting.beginGroup("WindowProperties");
    setting.setValue("size", this->size());
    setting.setValue("pos", this->pos());
    setting.setValue("state", this->saveState());
    setting.setValue("headerState", ui->empTableView->horizontalHeader()->saveState());
    setting.endGroup();
}

void empView::storeSettings()
{
    QSettings setting;
    setting.beginGroup("database");
    setting.setValue("dbpath", QSqlDatabase::database().databaseName());
    setting.endGroup();
}

void empView::showAboutDialog()
{
    QString title;
    QString message;
    title.append(tr("About "));
    title.append(QApplication::applicationDisplayName());
    message.append(QApplication::applicationDisplayName() + " - Version: " + QApplication::applicationVersion());
    message.append(tr("\n\nDeveloped by Simon Himmelbauer"));
    QMessageBox::about(this,title, message);
}

empView::~empView()
{
    storeAppState();
    storeSettings();
    delete ui;
}
