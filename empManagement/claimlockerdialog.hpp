#ifndef CLAIMLOCKERDIALOG_HPP
#define CLAIMLOCKERDIALOG_HPP

#include <QDialog>
#include "dependencies.hpp"

namespace Ui {
class claimLockerDialog;
}

class claimLockerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit claimLockerDialog(QWidget *parent = 0);
    ~claimLockerDialog();

private:
    Ui::claimLockerDialog *ui;

    QList<libemployee::Locker> currentLockers;

public slots:
    void show(libemployee::Locker::SEX sex);
    void accept();

signals:
    void accepted(int lockerID);
};

#endif // CLAIMLOCKERDIALOG_HPP
