#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <QUndoCommand>
#include "dependencies.hpp"

class addCommand : public QUndoCommand
{
private:
    libemployee::Employee emp;
public:
    enum { Id = -1 };

    addCommand(const libemployee::Employee *emp, QUndoCommand *parent = 0);
    void undo() override;
    void redo() override;
    int id() const override { return Id; }
};

class updateCommand : public QUndoCommand
{
private:
    libemployee::Employee oldEmp;
    libemployee::Employee newEmp;
public:
    enum { Id = -1 };

    updateCommand(const libemployee::Employee *oldEmp, const libemployee::Employee *newEmp, QUndoCommand *parent = 0);
    void undo() override;
    void redo() override;
    int id() const override { return Id; }
};

class deleteCommand : public QUndoCommand
{
private:
    libemployee::Employee removedEmp;
public:
    enum { Id = -1 };

    deleteCommand(const libemployee::Employee *emp, QUndoCommand *parent = 0);
    void undo() override;
    void redo() override;
    int id() const override { return Id; }
};

class releaseCommand : public QUndoCommand
{
private:
    libemployee::Employee emp;
    int lockId;
public:
    enum { Id = -1 };

    releaseCommand(const libemployee::Employee *emp, QUndoCommand *parent = 0);
    void undo() override;
    void redo() override;
    int id() const override { return Id; }
};

class claimCommand : public QUndoCommand
{
private:
    libemployee::Employee emp;
    int lockId;
public:
    enum { Id = -1 };

    claimCommand(const libemployee::Employee *emp, const int lockId, QUndoCommand *parent = 0);
    void undo() override;
    void redo() override;
    int id() const override { return Id; }
};

#endif // COMMANDS_HPP
