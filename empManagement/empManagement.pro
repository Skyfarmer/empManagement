#-------------------------------------------------
#
# Project created by QtCreator 2017-02-26T14:10:35
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = empManagement
TEMPLATE = app

VERSION = 1.0

CONFIG += link_prl

LIBS += ../lbemployee/liblbemployee.a

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        empview.cpp \
    addemployeedialog.cpp \
    csqlquerymodel.cpp \
    claimlockerdialog.cpp \
    appsettings.cpp \
    commands.cpp

HEADERS  += empview.hpp \
    addemployeedialog.hpp \
    dependencies.hpp \
    csqlquerymodel.hpp \
    claimlockerdialog.hpp \
    appsettings.hpp \
    commands.hpp

FORMS    += empview.ui \
    addemployeedialog.ui \
    claimlockerdialog.ui

DISTFILES += \
    Notes.txt

RESOURCES += \
    resources.qrc
