#ifndef EMPLOYEE_HPP
#define EMPLOYEE_HPP
#include <QString>

namespace libemployee
{
    class Employee
    {
    public:
        enum SEX
        {
            FEMALE,
            MALE,
            NONE
        };

    private:
        int userId = -1;         // card id
        int lockerId = -1;
        SEX sex = SEX::NONE;
        QString title;
        QString firstName;
        QString lastName;
        QString department;
    public:

        int getID() const;
        int getLockerID() const;
        SEX getSex() const;
        QString getTitle() const;
        QString getFirstName() const;
        QString getLastName() const;
        QString getDepartment() const;

        void setID(int id);
        void setLockerID(int lockerId);
        void setSex(SEX sex);
        void setTitle(QString title);
        void setFirstName(QString firstName);
        void setLastName(QString lastName);
        void setDepartment(QString department);
    };
}
#endif // EMPLOYEE_HPP
