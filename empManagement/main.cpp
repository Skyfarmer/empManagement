#include "empview.hpp"
#include <QApplication>
#include <QMessageBox>
#include <QFileInfo>
#include <QObject>
#include <QLockFile>
#include <QTranslator>
#include <QLibraryInfo>

#include "appsettings.hpp"

enum ERROR
{
    DB_NOT_EXISTENT,
    DB_NOT_WRITEABLE,
    PATH_NOT_FILE,
    RESOURCE_LOCKED
};

void checkDatabase(QString path)
{
    QFileInfo db(path);
    // Check if the database even exists
    if(!db.exists())
        throw ERROR::DB_NOT_EXISTENT;
    else if(!db.isFile())
        throw ERROR::PATH_NOT_FILE;
    else if(!db.isWritable())
        throw ERROR::DB_NOT_WRITEABLE;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setApplicationName("empManagement");
    QApplication::setApplicationDisplayName("Employee Management Program");
    QApplication::setApplicationVersion("1.0");
    QApplication::setOrganizationName("skynet");
    QApplication::setWindowIcon(QIcon(":/icons/app.png"));
    QApplication::setQuitOnLastWindowClosed(true);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&qtTranslator);

    QTranslator empTranslator;
    if(QLocale::system().language() == QLocale::German)
    {
        empTranslator.load(":/langs/translations_de");
        a.installTranslator(&empTranslator);
    }

    QString db(Settings::readSetting(Settings::DB_NAME).toString());

    try
    {
        checkDatabase(db);
    }
    catch(ERROR error)
    {
        switch(error)
        {
        case DB_NOT_EXISTENT:
            QApplication::beep();
            QMessageBox::critical(nullptr, QObject::tr("Database not found"), QObject::tr("The database file could not be found. Please create it first or change the location of the database in the settings file."));
            return ERROR::DB_NOT_EXISTENT;
        case DB_NOT_WRITEABLE:
            QApplication::beep();
            QMessageBox::critical(nullptr, QObject::tr("Database is read-only"), QObject::tr("The database file could be found, however, it seems to appear to be read-only. Check your permissions or move the database to a writeable location."));
            return ERROR::DB_NOT_WRITEABLE;
        case PATH_NOT_FILE:
            QApplication::beep();
            QMessageBox::critical(nullptr, QObject::tr("Path is not a file"), QObject::tr("The specified path in the settings is not a file."));
            return ERROR::PATH_NOT_FILE;
        default:
            break;
        }
    }

    QLockFile lock(QFileInfo(db).absolutePath() + "/.lock");
    lock.setStaleLockTime(0);
    if(!lock.tryLock())
    {
        if(lock.error() == QLockFile::LockFailedError)
        {
            QApplication::beep();
            QString message;
            qint64 pid;
            lock.getLockInfo(&pid, nullptr, nullptr);
            message.append(QObject::tr("There is already an instance (PID: "));
            message.append(QString::number(pid));
            message.append(QObject::tr(") running."));
            QMessageBox::critical(nullptr, QObject::tr("Database is locked"), message);
            return ERROR::RESOURCE_LOCKED;
        }
    }

    empView w(db);
    w.show();


    return a.exec();
}
