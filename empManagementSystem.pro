TEMPLATE = subdirs

SUBDIRS += lbemployee \
        empManagement

empManagement.depends = lbemployee

TRANSLATIONS = empManagement/translations_de.ts
