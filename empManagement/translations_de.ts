<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_GB">
<context>
    <name>CSqlQueryModel</name>
    <message>
        <source>Man</source>
        <translation>Mann</translation>
    </message>
    <message>
        <source>Woman</source>
        <translation>Frau</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Database not found</source>
        <translation>Datenbank nicht gefunden</translation>
    </message>
    <message>
        <source>The database file could not be found. Please create it first or change the location of the database in the settings file.</source>
        <translation>Die Datenbank konnte nicht gefunden werden. Eine neue muss erstellt werden oder der Pfad zur Datei kann in der Einstellungsdatei geändert werden.</translation>
    </message>
    <message>
        <source>Database is read-only</source>
        <translation>Datenbank kann nur gelesen werden</translation>
    </message>
    <message>
        <source>The database file could be found, however, it seems to appear to be read-only. Check your permissions or move the database to a writeable location.</source>
        <translation>Die Datenbank wurde gefunden, jedoch kann sie nicht beschrieben werden. Überprüfen Sie die Berechtigungen oder bewegen Sie die Datenbank in einen beschreibbaren Ordner.</translation>
    </message>
    <message>
        <source>Path is not a file</source>
        <translation>Pfad ist keine Datei</translation>
    </message>
    <message>
        <source>The specified path in the settings is not a file.</source>
        <translation>Der angegebene Pfad in den Einstellungen ist keine Datei.</translation>
    </message>
    <message>
        <source>There is already an instance (PID: </source>
        <translation>Eine Instanz mit der Prozess-ID </translation>
    </message>
    <message>
        <source>) running.</source>
        <translation>läuft bereits.</translation>
    </message>
    <message>
        <source>Database is locked</source>
        <translation>Datenbank bereits in Verwendung</translation>
    </message>
    <message>
        <source>Remove employee %1 %2</source>
        <translation>Entferne Mitarbeiter %1 %2</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Unbekannter Fehler</translation>
    </message>
    <message>
        <source>An unknown error has occured. ERROR CODE: </source>
        <translation>Ein unbekannter Fehler ist aufgetreten. FEHLER-CODE: </translation>
    </message>
    <message>
        <source>Add employee %1 %2</source>
        <translation>Füge Mitarbeiter %1 %2 hinzu</translation>
    </message>
    <message>
        <source>Employee already existent</source>
        <translation>Mitarbeiter existiert bereits</translation>
    </message>
    <message>
        <source>The user ID is not available, the employee is probably already in the database.</source>
        <translation>The angegebene Karten-ID ist nicht verfügbar, wahrscheinlich existiert der Mitarbeiter bereits in der Datenbank.</translation>
    </message>
    <message>
        <source>Revert changes to employee %1 %2</source>
        <translation>Änderungen von Mitarbeiter %1 %2 widerrufen</translation>
    </message>
    <message>
        <source>Redo changes to employee %1 %2</source>
        <translation>Wiederhole Änderungen an Mitarbeiter %1 %2</translation>
    </message>
    <message>
        <source>Claim locker for %1 %2</source>
        <translation>Teile Spind %1 %2 zu</translation>
    </message>
    <message>
        <source>Release locker from %1 %2</source>
        <translation>Freigabe von Spind von %1 %2</translation>
    </message>
</context>
<context>
    <name>addEmployeeDialog</name>
    <message>
        <source>Add an employee</source>
        <translation>Mitarbeiter hinzufügen</translation>
    </message>
    <message>
        <source>card ID:</source>
        <translation>Karten-ID:</translation>
    </message>
    <message>
        <source>Please enter identification number</source>
        <translation>Identifikationsnummer eingeben</translation>
    </message>
    <message>
        <source>Title:</source>
        <translation>Title:</translation>
    </message>
    <message>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <source>Please enter first name</source>
        <translation>Vornamen eingeben</translation>
    </message>
    <message>
        <source>Last Name:</source>
        <translation>Nachname:</translation>
    </message>
    <message>
        <source>Please enter last name</source>
        <translation>Nachnamen eingeben</translation>
    </message>
    <message>
        <source>Sex:</source>
        <translation>Geschlecht:</translation>
    </message>
    <message>
        <source>Man</source>
        <translation>Mann</translation>
    </message>
    <message>
        <source>Woman</source>
        <translation>Frau</translation>
    </message>
    <message>
        <source>Department:</source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <source>Locker:</source>
        <translation>Spind:</translation>
    </message>
    <message>
        <source>No locker</source>
        <translation>Kein Spind</translation>
    </message>
    <message>
        <source>Room: </source>
        <translation>Raum: </translation>
    </message>
    <message>
        <source>/Number: </source>
        <translation>/Nummer: </translation>
    </message>
    <message>
        <source>Missing user ID</source>
        <translation>Fehlende Karten-ID</translation>
    </message>
    <message>
        <source>Please enter a valid user ID</source>
        <translation>Bitte geben Sie eine gültige Karten-ID ein</translation>
    </message>
    <message>
        <source>Missing first name</source>
        <translation>Fehlender Vorname</translation>
    </message>
    <message>
        <source>Please enter a first name</source>
        <translation>Bitte geben Sie einen Vornamen ein</translation>
    </message>
    <message>
        <source>Missing last name</source>
        <translation>Fehlender Nachname</translation>
    </message>
    <message>
        <source>Please enter a last name</source>
        <translation>Bitte geben Sie einen Nachnamen ein</translation>
    </message>
</context>
<context>
    <name>claimLockerDialog</name>
    <message>
        <source>Claim Locker</source>
        <translation>Spind zuteilen</translation>
    </message>
    <message>
        <source>Locker:</source>
        <translation>Spind:</translation>
    </message>
    <message>
        <source>Room: </source>
        <translation>Raum: </translation>
    </message>
    <message>
        <source>/Number: </source>
        <translation>/Nummer: </translation>
    </message>
</context>
<context>
    <name>empView</name>
    <message>
        <source>File</source>
        <translation type="vanished">Datei</translation>
    </message>
    <message>
        <source>Employee</source>
        <translation>Mitarbeiter</translation>
    </message>
    <message>
        <source>Locker</source>
        <translation>Spind</translation>
    </message>
    <message>
        <source>Add Employee</source>
        <translation>Mitarbeiter hinzufügen</translation>
    </message>
    <message>
        <source>Add a new employee to the database</source>
        <translation>Einen neuen Mitarbeiter der Datenbank hinzufügen</translation>
    </message>
    <message>
        <source>Update Employee</source>
        <translation>Mitarbeiterinfo aktualisieren</translation>
    </message>
    <message>
        <source>Update information about a particular employee in the database</source>
        <translation>Informationen über einen Mitarbeiter aktualisieren</translation>
    </message>
    <message>
        <source>Remove Employee</source>
        <translation>Mitarbeiter entfernen</translation>
    </message>
    <message>
        <source>Remove the selected employee from the database</source>
        <translation>Einen Mitarbeiter aus der Datenbank entfernen</translation>
    </message>
    <message>
        <source>Release Locker</source>
        <translation>Sping freigeben</translation>
    </message>
    <message>
        <source>Release a locker so that it can be occupied by another employee</source>
        <translation>Einen Spind freigeben, um ihn für andere Mitarbeiter zugänglich zu machen</translation>
    </message>
    <message>
        <source>Claim Locker</source>
        <translation>Spind zuteilen</translation>
    </message>
    <message>
        <source>Claim a locker for the selected employee</source>
        <translation>Dem ausgewählten Mitarbeiter einen Spind zuteilen</translation>
    </message>
    <message>
        <source>User ID</source>
        <translation>Karten-ID</translation>
    </message>
    <message>
        <source>Room / Locker</source>
        <translation>Raum / Spind</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Geschlecht</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Title</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <source>Department</source>
        <translation>Abteilung</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <source>by ID</source>
        <translation>nach Karten-ID</translation>
    </message>
    <message>
        <source>by first name</source>
        <translation>nach Vorname</translation>
    </message>
    <message>
        <source>by last name</source>
        <translation>nach Nachname</translation>
    </message>
    <message>
        <source>by department</source>
        <translation>nach Abteilung</translation>
    </message>
    <message>
        <source>Employee already existent</source>
        <translation type="vanished">Mitarbeiter existiert bereits</translation>
    </message>
    <message>
        <source>The user ID is not available, the employee is probably already in the database.</source>
        <translation type="vanished">The angegebene Karten-ID ist nicht verfügbar, wahrscheinlich existiert der Mitarbeiter bereits in der Datenbank.</translation>
    </message>
    <message>
        <source>The user ID is not available, the employee is probably already in the database</source>
        <translation type="vanished">The angegebene Karten-ID ist nicht verfügbar, wahrscheinlich existiert der Mitarbeiter bereits in der Datenbank.</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Unbekannter Fehler</translation>
    </message>
    <message>
        <source>An unknown error has occured. ERROR CODE: </source>
        <translation type="vanished">Ein unbekannter Fehler ist aufgetreten. FEHLER-CODE: </translation>
    </message>
    <message>
        <source>You are about to remove </source>
        <translation>Der Mitarbeiter </translation>
    </message>
    <message>
        <source> with the user ID </source>
        <translation> mit der Karten-ID </translation>
    </message>
    <message>
        <source>.</source>
        <translation> wird nach Bestätigung freigegeben.</translation>
    </message>
    <message>
        <source>Removing employee</source>
        <translation>Mitarbeiter entfernen</translation>
    </message>
    <message>
        <source>Employee Database</source>
        <translation>Mitarbeiterdatenbank</translation>
    </message>
    <message>
        <source>.</source>
        <comment>REMOVAL</comment>
        <translation> wird nach Bestätigung entfernt.</translation>
    </message>
    <message>
        <source>You are about to release a locker from </source>
        <translation>Der Spind von </translation>
    </message>
    <message>
        <source>Releasing locker</source>
        <translation>Sping freigeben</translation>
    </message>
    <message>
        <source>You are about to claim a locker for </source>
        <translation>Dem Mitarbeiter </translation>
    </message>
    <message>
        <source>.</source>
        <comment>CLAIMING</comment>
        <translation> wird ein Spind zugeteilt.</translation>
    </message>
    <message>
        <source>Claiming locker</source>
        <translation>Spind zuteilen</translation>
    </message>
    <message>
        <source>Employee already has locker</source>
        <translation>Mitarbeiter wurde bereits Spind zugeteilt</translation>
    </message>
    <message>
        <source>The selected employee already has a locker. We can automatically release it.</source>
        <translation>Der angegebene Mitarbeiter ist bereits in Besitz eines Spindes. Letzterer kann automatisch wieder freigegeben werden.</translation>
    </message>
    <message>
        <source>empView</source>
        <translation>empView</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>About this program</source>
        <translation>Über dieses Programm</translation>
    </message>
    <message>
        <source>Undo: </source>
        <translation>Widerrufen: </translation>
    </message>
    <message>
        <source>Redo: </source>
        <translation>Wiederholen: </translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>About </source>
        <translation>Über </translation>
    </message>
    <message>
        <source>

Developed by Simon Himmelbauer</source>
        <translation>

Entwickelt von Simon Himmelbauer</translation>
    </message>
</context>
</TS>
