#ifndef APPSETTINGS_HPP
#define APPSETTINGS_HPP

#include <QVariant>

namespace Settings
{
    enum SETTINGS
    {
        DB_NAME
    };

    QVariant readSetting(SETTINGS prop);
}

#endif // APPSETTINGS_HPP
