#include "database.hpp"
#include <QSqlDatabase>
#include <QDebug>
using namespace libemployee;

int main(int argc, char *argv[])
{
    try
    {
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("Test.db");
    Database members(db);

    Employee Achim;
    Achim.setID(3455);
    Achim.setFirstName("Achim");
    Achim.setLastName("Himmelbauer");
    Achim.setSex(Employee::MALE);
    Achim.setDepartment("Test");
    Achim.setlockerID(2);

    members.updateUser(Achim);
    members.releaseLocker(3455);
    }
    catch(LIBEMP_ERROR error)
    {
        qDebug() << error;
    }

    return 0;
}
