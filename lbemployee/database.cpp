#include "database.hpp"
#include <QSqlQuery>
#include <QVariant>
//#include <QDebug>
using namespace libemployee;

Database::Database(QSqlDatabase db)
{
    // Check whether the database name has been specified
    if(db.databaseName().isEmpty())
    {
        throw LIBEMP_ERROR::LIBEMP_DATABASE_NAME_MISSING;
    }
    databaseName = QString::number(rand());

    QSqlDatabase::cloneDatabase(db, databaseName);
    QSqlDatabase::database(databaseName);
    // Opening connection and checking whether the database has the employees and lockers tables
    if(!(db.open()))
    {
        throw LIBEMP_ERROR::LIBEMP_DATABASE_CONNECTION_ERROR;
    }

    QStringList tableNames;
    bool employeesExists = false;
    bool lockersExists = false;
    tableNames = db.tables();
    for(int i = 0; i < tableNames.length(); i++)
    {
        if(tableNames.at(i) == "employees")
            employeesExists = true;
        else if(tableNames.at(i) == "lockers")
            lockersExists = true;
    }
    if(!employeesExists || !lockersExists)
    {
        cleanUp();
        throw LIBEMP_ERROR::LIBEMP_DATABASE_INCOMPATIBLE;
    }
}

Database::~Database()
{
    cleanUp();
}

void Database::addUser(const Employee &emp)
{
    // Make some preparations and check if the user already exists
    if(prepareUserMod(emp))
        throw LIBEMP_ERROR::LIBEMP_EMPLOYEE_EXISTS;

    QSqlQuery query(db);
    query.prepare("INSERT INTO employees(userID, lockID, isMasc, title, firstName, lastName, department) VALUES (:userID, :lockID, :isMasc, :title, :firstName, :lastName, :department);");
    query.bindValue(":userID", emp.getID());
    if(emp.getLockerID() == -1)
        ;
    else
        query.bindValue(":lockID", emp.getLockerID());
    switch(emp.getSex())
    {
    case Employee::MALE:
        query.bindValue(":isMasc", 1);
        break;
    case Employee::FEMALE:
        query.bindValue(":isMasc", 0);
        break;
    case Employee::NONE:
            break;
    }
    query.bindValue(":title", emp.getTitle());
    query.bindValue(":firstName", emp.getFirstName());
    query.bindValue(":lastName", emp.getLastName());
    query.bindValue(":department", emp.getDepartment());
    query.exec();

    if(!(query.isActive()))
        throw LIBEMP_ERROR::LIBEMP_DATABASE_EXECUTION_ERROR;
}

void Database::updateUser(const Employee &emp)
{
    if(!(prepareUserMod(emp)))
        throw LIBEMP_ERROR::LIBEMP_EMPLOYEE_NOT_EXISTENT;

    QString cmd;
    cmd.append("UPDATE employees SET lockID = ");
    if(emp.getLockerID() == -1)
        cmd.append("NULL");
    else
        cmd.append(QString::number(emp.getLockerID()));
    cmd.append(" , title = '" + emp.getTitle());
    cmd.append("' , firstName = '" + emp.getFirstName());
    cmd.append("' , lastName = '" + emp.getLastName());
    cmd.append("' , department = '" + emp.getDepartment());
    cmd.append("' WHERE userID = " + QString::number(emp.getID()));
    cmd.append(";");
    QSqlQuery query(db);
    query.prepare(cmd);
    query.exec();

    if(!(query.isActive()))
        throw LIBEMP_ERROR::LIBEMP_DATABASE_EXECUTION_ERROR;
}

void Database::removeUser(const int userID)
{
    if(!(isUserExistent(userID)))
        throw LIBEMP_ERROR::LIBEMP_EMPLOYEE_NOT_EXISTENT;

    QSqlQuery query(db);
    query.prepare("DELETE FROM employees WHERE userID = " + QString::number(userID) + ";");
    query.exec();

    if(!(query.isActive()))
        throw LIBEMP_ERROR::LIBEMP_DATABASE_EXECUTION_ERROR;
}

void Database::releaseLocker(const int userID)
{
    if(!(isUserExistent(userID)))
        throw LIBEMP_ERROR::LIBEMP_EMPLOYEE_NOT_EXISTENT;

    QSqlQuery query(db);
    query.prepare("UPDATE employees SET lockID = NULL WHERE userID = " + QString::number(userID) + ";");
    query.exec();

    if(!(query.isActive()))
        throw LIBEMP_ERROR::LIBEMP_DATABASE_EXECUTION_ERROR;
}

void Database::fetchLocker(QList<Locker> *list, QString criteria)
{
    QString cmd;
    cmd.append("SELECT * FROM lockers ");
    cmd.append(criteria);
    cmd.append(";");
    QSqlQuery query(db);
    query.prepare(cmd);
    query.exec();

    if(!(query.isActive()))
        throw LIBEMP_ERROR::LIBEMP_DATABASE_EXECUTION_ERROR;

    if(query.next() == false )
        return;

    while(true)
    {
        int intID;
        int num;
        int room;
        Locker::SEX sex;

        intID = query.value(0).toInt();
        num = query.value(1).toInt();
        room = query.value(2).toInt();
        if(query.value(3) == 1)
            sex = Locker::MALE;
        else
            sex = Locker::FEMALE;

        Locker tempLock(intID, num, room, sex);
        list->append(tempLock);
        if(query.next() == false)
            break;
    }
}

void Database::fetchEmployee(QList<Employee> *list, QString criteria)
{
    QString cmd;
    cmd.append("SELECT * FROM employees ");
    cmd.append(criteria);
    cmd.append(";");
    QSqlQuery query(db);
    query.prepare(cmd);
    query.exec();

    if(!(query.isActive()))
        throw LIBEMP_ERROR::LIBEMP_DATABASE_EXECUTION_ERROR;

    if(query.next() == false)
        return;

    while(true)
    {
        int userID;
        int lockID;
        Employee::SEX sex;
        QString title;
        QString firstName;
        QString lastName;
        QString department;

        userID = query.value(0).toInt();
        if(query.value(1).isNull())
            lockID = -1;
        else
            lockID = query.value(1).toInt();
        if(query.value(2) == 1)
            sex = Employee::MALE;
        else
            sex = Employee::FEMALE;
        title = query.value(3).toString();
        firstName = query.value(4).toString();
        lastName = query.value(5).toString();
        department = query.value(6).toString();

        Employee tempEmp;
        tempEmp.setID(userID);
        tempEmp.setLockerID(lockID);
        tempEmp.setSex(sex);
        tempEmp.setTitle(title);
        tempEmp.setFirstName(firstName);
        tempEmp.setLastName(lastName);
        tempEmp.setDepartment(department);
        list->append(tempEmp);
        if(query.next() == false)
            break;
    }
}

//************PRIVATE FUNCTIONS*************
void Database::cleanUp()
{
    db.close();
}

bool Database::isUserValid(const Employee &emp)
{
    // ID: CHECK
    if(emp.getID() == -1)
        return false;
    if(emp.getSex() == Employee::NONE)
        return false;
    if(emp.getFirstName().isEmpty())
        return false;
    if(emp.getLastName().isEmpty())
        return false;
    return true;
}

bool Database::isUserExistent(const int userID)
{
    QList<Employee> tempEmp;
    fetchEmployee(&tempEmp, QString("WHERE userID = '" + QString::number(userID) + "'"));

    if(tempEmp.isEmpty())
        return false;
    return true;
}

bool Database::prepareUserMod(const Employee &emp)
{
    // Are all required fields occupied?
    if(!(isUserValid(emp)))
        throw LIBEMP_ERROR::LIBEMP_EMPLOYEE_INFO_MISSING;

    // If user requests a locker, is it available?
    if(emp.getLockerID() != -1 )
    {
        QList<Employee> tempEmp;
        QList<Locker> tempLock;
        (fetchEmployee(&tempEmp, QString("WHERE lockID = " + QString::number(emp.getLockerID()) + " AND userID <> " + QString::number(emp.getID()))));

        if(!(tempEmp.isEmpty()))
        {
            throw LIBEMP_ERROR::LIBEMP_EMPLOYEE_LOCKER_NOT_AVAILABLE;
        }
        // Does the locker match the user's sex and does it even exist?
        fetchLocker(&tempLock, QString("WHERE intID = " + QString::number(emp.getLockerID())));

        if(tempLock.isEmpty())
            throw LIBEMP_ERROR::LIBEMP_EMPLOYEE_LOCKER_NOT_EXISTENT;

        if(tempLock.first().getSex() != emp.getSex())
            throw LIBEMP_ERROR::LIBEMP_EMPLOYEE_LOCKER_INCOMPATIBLE;
    }
    // CHECK

    // Does the user already exist?
    return isUserExistent(emp.getID());
}
