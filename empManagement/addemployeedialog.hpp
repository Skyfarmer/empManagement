#ifndef ADDEMPLOYEEDIALOG_HPP
#define ADDEMPLOYEEDIALOG_HPP

#include <QDialog>
#include "dependencies.hpp"

namespace Ui {
class addEmployeeDialog;
}

class addEmployeeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addEmployeeDialog(QWidget *parent = 0);
    ~addEmployeeDialog();

private:
    Ui::addEmployeeDialog *ui;

    QList<libemployee::Locker> currentLockers;
    int updateLockID = -1;

public slots:
    void show();
    void show(const libemployee::Employee &emp);
    void updateLockers(int index);
    void accept();

signals:
    void accepted(libemployee::Employee emp);
};

#endif // ADDEMPLOYEEDIALOG_HPP
