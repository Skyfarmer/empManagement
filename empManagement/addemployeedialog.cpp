#include <QSqlDatabase>
#include <QStringList>
#include <QIntValidator>
#include <QMessageBox>

#include "addemployeedialog.hpp"
#include "ui_addemployeedialog.h"

addEmployeeDialog::addEmployeeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addEmployeeDialog)
{
    ui->setupUi(this);


    // Form some relationships
    connect(ui->comboSex, SIGNAL(currentIndexChanged(int)), this, SLOT(updateLockers(int)));

}

void addEmployeeDialog::show()
{ 
    // Clear all fields
    ui->lineID->clear();
    ui->comboTitle->clear();
    ui->lineFirstName->clear();
    ui->lineLastName->clear();
    ui->comboDepartment->clear();
    ui->comboLocker->clear();
    // Let's fill it with all titles, so that the user does not need to reenter them, reducing the probability of errors
    libemployee::Database db(QSqlDatabase::database());
    QList<libemployee::Employee> tempEmp;
    QStringList content;

    ui->lineID->setValidator(new QIntValidator); // User can only type integers

    db.fetchEmployee(&tempEmp);
    // First all titles
    for(int i = 0; i < tempEmp.count(); i++ )
    {
        content.append(tempEmp.at(i).getTitle());
    }
    content.removeDuplicates();
    content.sort();
    if(!content.contains(""))
        content.insert(0, "");
    ui->comboTitle->addItems(content);
    content.clear();

    // Now the same for the departments
    for(int i = 0; i < tempEmp.count(); i++)
        content.append(tempEmp.at(i).getDepartment());
    content.removeDuplicates();
    content.sort();
    if(!content.contains(""))
        content.insert(0, "");
    ui->comboDepartment->addItems(content);
    content.clear();

    // Finally show the correct lockers
    ui->comboSex->setCurrentIndex(0);
    updateLockers(0);

    QDialog::show();
}

void addEmployeeDialog::show(const libemployee::Employee &emp)
{
    // Clear all fields
    ui->comboTitle->clear();
    ui->comboDepartment->clear();
    ui->comboLocker->clear();

    // Let's fill it with all titles, so that the user does not need to reenter them, reducing the probability of errors
    libemployee::Database db(QSqlDatabase::database());
    QList<libemployee::Employee> tempEmp;
    QStringList content;

    ui->lineID->setValidator(new QIntValidator); // User can only type integers
    ui->lineID->setText(QString::number(emp.getID()));
    ui->lineID->setDisabled(true);

    db.fetchEmployee(&tempEmp);
    // First all titles
    for(int i = 0; i < tempEmp.count(); i++ )
    {
        content.append(tempEmp.at(i).getTitle());
    }
    content.removeDuplicates();
    content.sort();
    if(!content.contains(""))
        content.insert(0, "");
    ui->comboTitle->addItems(content);
    ui->comboTitle->setCurrentIndex(content.indexOf(emp.getTitle())); // Select the correct title of the employee
    content.clear();

    // Now the same for the departments
    for(int i = 0; i < tempEmp.count(); i++)
        content.append(tempEmp.at(i).getDepartment());
    content.removeDuplicates();
    content.sort();
    if(!content.contains(""))
        content.insert(0, "");
    ui->comboDepartment->addItems(content);
    ui->comboDepartment->setCurrentIndex(content.indexOf(emp.getDepartment())); // Select the correct department of the employee
    content.clear();

    ui->lineFirstName->setText(emp.getFirstName());
    ui->lineLastName->setText(emp.getLastName());

    if(emp.getSex() == libemployee::Employee::MALE)
        ui->comboSex->setCurrentIndex(0);
    else
        ui->comboSex->setCurrentIndex(1);
    ui->comboSex->setDisabled(true);

    // Finally show the correct lockers
    ui->comboLocker->setHidden(true);
    ui->comboLocker->setCurrentIndex(0);
    ui->labelLocker->setHidden(true);

    updateLockID = emp.getLockerID();

    QDialog::show();
}

void addEmployeeDialog::updateLockers(int index)
{
    bool isMasc;
    libemployee::Database db(QSqlDatabase::database());
    QString condition;
    QList<libemployee::Employee> tempEmp;
    QList<int> tempInt;

    if(index == 0) isMasc = true;
    else isMasc = false;

    condition.append("WHERE isMasc = ");
    if(isMasc)
        condition.append("1");
    else
        condition.append("0");

    ui->comboLocker->clear();
    currentLockers.clear();

    // Let's check if lockers are occupied or not
    db.fetchEmployee(&tempEmp);
    for(int i = 0; i < tempEmp.count(); i++)
        tempInt.append(tempEmp.at(i).getLockerID());

    db.fetchLocker(&currentLockers, condition);
    ui->comboLocker->addItem(tr("No locker"));
    for(int i = 0; i < currentLockers.count(); i++)
    {
        if(tempInt.contains(currentLockers.at(i).getintID()))
        {
            currentLockers.removeAt(i);
            i--;
            continue;
        }
        QString temp;
        temp.append(tr("Room: ") + QString::number(currentLockers.at(i).getRoom()));
        temp.append(tr("/Number: ") + QString::number(currentLockers.at(i).getNum()));
        ui->comboLocker->addItem(temp);
    }
}

void addEmployeeDialog::accept()
{
    // Construct our employee
    libemployee::Employee tempEmp;
    if(ui->lineID->text().isEmpty())
    {
        QMessageBox::critical(this, tr("Missing user ID"), tr("Please enter a valid user ID"));
        ui->lineID->setFocus();
        return;
    }
    tempEmp.setID(ui->lineID->text().toInt());
    if(ui->comboLocker->currentIndex() != 0 && ui->comboLocker->count() != 0)
        tempEmp.setLockerID(currentLockers.at(ui->comboLocker->currentIndex() - 1).getintID());
    if(updateLockID != -1)
        tempEmp.setLockerID(updateLockID);
    if(ui->comboSex->currentIndex() == 0)
        tempEmp.setSex(libemployee::Employee::MALE);
    else
        tempEmp.setSex(libemployee::Employee::FEMALE);
    tempEmp.setTitle(ui->comboTitle->currentText());
    if(ui->lineFirstName->text().isEmpty())
    {
        QMessageBox::critical(this, tr("Missing first name"), tr("Please enter a first name"));
        ui->lineFirstName->setFocus();
        return;
    }
    tempEmp.setFirstName(ui->lineFirstName->text());
    if(ui->lineLastName->text().isEmpty())
    {
        QMessageBox::critical(this, tr("Missing last name"), tr("Please enter a last name"));
        ui->lineLastName->setFocus();
        return;
    }
    tempEmp.setLastName(ui->lineLastName->text());
    tempEmp.setDepartment(ui->comboDepartment->currentText());

    emit accepted(tempEmp);
}

addEmployeeDialog::~addEmployeeDialog()
{
    delete ui;
}
