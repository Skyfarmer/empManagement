#ifndef CSQLQUERYMODEL_HPP
#define CSQLQUERYMODEL_HPP

#include <QSqlQueryModel>
#include <dependencies.hpp>

class CSqlQueryModel : public QSqlQueryModel
{
    Q_OBJECT
private:
    QList<libemployee::Locker> lockdb;
public:
    CSqlQueryModel(QList<libemployee::Locker> &lockdb, QObject *parent = 0);
    QVariant data(const QModelIndex &item, int role) const;
};

#endif // CSQLQUERYMODEL_HPP
