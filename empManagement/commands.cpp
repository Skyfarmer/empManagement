#include "commands.hpp"
#include <QMessageBox>
#include <QObject>


//****BEGIN: ADD COMMAND******
addCommand::addCommand(const libemployee::Employee *emp, QUndoCommand *parent) : QUndoCommand(parent)
{
    this->emp = *emp;
    QString text;
    text.append(QObject::tr("Remove employee %1 %2"));
    setText(text.arg(emp->getFirstName()).arg(emp->getLastName()));
}

void addCommand::undo()
{
    libemployee::Database db(QSqlDatabase::database());
    try
    {
        db.removeUser(emp.getID());
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch (error)
        {
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
    }

    QString text;
    text.append(QObject::tr("Add employee %1 %2"));
    setText(text.arg(emp.getFirstName()).arg(emp.getLastName()));

}

void addCommand::redo()
{
    libemployee::Database db(QSqlDatabase::database());
    try
    {
        db.addUser(emp);
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch(error)
        {
        case libemployee::LIBEMP_EMPLOYEE_EXISTS:
            QMessageBox::critical(nullptr, QObject::tr("Employee already existent"), QObject::tr("The user ID is not available, the employee is probably already in the database."));
            break;
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
        return;
    }
    QString text;
    text.append(QObject::tr("Remove employee %1 %2"));
    setText(text.arg(emp.getFirstName()).arg(emp.getLastName()));
}

// *****END: ADD COMMAND******

// *****BEGIN: UPDATE COMMAND*****
updateCommand::updateCommand(const libemployee::Employee *oldEmp, const libemployee::Employee *newEmp, QUndoCommand *parent) : QUndoCommand(parent)
{
    this->oldEmp = *oldEmp;
    this->newEmp = *newEmp;
    QString text;
    text.append(QObject::tr("Revert changes to employee %1 %2"));
    setText(text.arg(oldEmp->getFirstName()).arg(oldEmp->getLastName()));
}

void updateCommand::undo()
{
    libemployee::Database db(QSqlDatabase::database());
    try
    {
        db.updateUser(oldEmp);
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch (error)
        {
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
        return;
    }
    QString text;
    text.append(QObject::tr("Redo changes to employee %1 %2"));
    setText(text.arg(oldEmp.getFirstName()).arg(oldEmp.getLastName()));
}

void updateCommand::redo()
{
    libemployee::Database db(QSqlDatabase::database());
    try
    {
        db.updateUser(newEmp);
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch (error)
        {
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
        return;
    }
    QString text;
    text.append(QObject::tr("Revert changes to employee %1 %2"));
    setText(text.arg(oldEmp.getFirstName()).arg(oldEmp.getLastName()));
}
//****END: UPDATE COMMAND

//****BEGIN: DELETE COMMAND
deleteCommand::deleteCommand(const libemployee::Employee *emp, QUndoCommand *parent) : QUndoCommand(parent)
{
    this->removedEmp = *emp;
    QString text;
    text.append(QObject::tr("Add employee %1 %2"));
    setText(text.arg(emp->getFirstName()).arg(emp->getLastName()));
}

void deleteCommand::undo()
{
    libemployee::Database db(QSqlDatabase::database());
    try
    {
        db.addUser(removedEmp);
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch(error)
        {
        case libemployee::LIBEMP_EMPLOYEE_EXISTS:
            QMessageBox::critical(nullptr, QObject::tr("Employee already existent"), QObject::tr("The user ID is not available, the employee is probably already in the database."));
            break;
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
        return;
    }
    QString text;
    text.append(QObject::tr("Remove employee %1 %2"));
    setText(text.arg(removedEmp.getFirstName()).arg(removedEmp.getLastName()));
}

void deleteCommand::redo()
{
    libemployee::Database db(QSqlDatabase::database());
    try
    {
        db.removeUser(removedEmp.getID());
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch (error)
        {
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
    }

    QString text;
    text.append(QObject::tr("Add employee %1 %2"));
    setText(text.arg(removedEmp.getFirstName()).arg(removedEmp.getLastName()));
}
//***END: DELETE COMMAND

//***BEGIN: RELEASE COMMAND
releaseCommand::releaseCommand(const libemployee::Employee *emp, QUndoCommand *parent) : QUndoCommand(parent)
{
    this->emp = *emp;
    this->lockId = emp->getLockerID();
    QString text;
    text.append(QObject::tr("Claim locker for %1 %2"));
    setText(text.arg(emp->getFirstName()).arg(emp->getLastName()));
}

void releaseCommand::undo()
{
    libemployee::Database db(QSqlDatabase::database());
    emp.setLockerID(lockId);
    try
    {
        db.updateUser(emp);
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch (error)
        {
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
    }
    QString text;
    text.append(QObject::tr("Release locker from %1 %2"));
    setText(text.arg(emp.getFirstName()).arg(emp.getLastName()));
}

void releaseCommand::redo()
{
    libemployee::Database db(QSqlDatabase::database());
    emp.setLockerID(-1);
    try
    {
        db.updateUser(emp);
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch (error)
        {
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
    }
    QString text;
    text.append(QObject::tr("Claim locker for %1 %2"));
    setText(text.arg(emp.getFirstName()).arg(emp.getLastName()));
}
//***END: RELEASE COMMAND

//***BEGIN: CLAIM COMMAND
claimCommand::claimCommand(const libemployee::Employee *emp, const int lockId, QUndoCommand *parent) : QUndoCommand(parent)
{
    this->emp = *emp;
    this->lockId = lockId;
    QString text;
    text.append(QObject::tr("Release locker from %1 %2"));
    setText(text.arg(emp->getFirstName()).arg(emp->getLastName()));
}

void claimCommand::undo()
{
    libemployee::Database db(QSqlDatabase::database());
    emp.setLockerID(-1);
    try
    {
        db.updateUser(emp);
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch (error)
        {
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
    }
    QString text;
    text.append(QObject::tr("Claim locker for %1 %2"));
    setText(text.arg(emp.getFirstName()).arg(emp.getLastName()));
}

void claimCommand::redo()
{
    libemployee::Database db(QSqlDatabase::database());
    emp.setLockerID(lockId);
    try
    {
        db.updateUser(emp);
    }
    catch(libemployee::LIBEMP_ERROR error)
    {
        switch (error)
        {
        default:
            QMessageBox::critical(nullptr, QObject::tr("Unknown error"), QString(QObject::tr("An unknown error has occured. ERROR CODE: ") + QString::number(error)));
        }
    }
    QString text;
    text.append(QObject::tr("Release locker from %1 %2"));
    setText(text.arg(emp.getFirstName()).arg(emp.getLastName()));
}
