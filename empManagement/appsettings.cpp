#include "appsettings.hpp"
#include <QSettings>
#include <QStandardPaths>

namespace Settings
{

    QVariant readSetting(SETTINGS prop)
    {
        QSettings setting;
        switch(prop)
        {
        case DB_NAME:
            QStringList paths;
            paths = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
            setting.beginGroup("database");
            return setting.value("dbpath", paths.first() + "/database.sqlite");
        }
    }
}
