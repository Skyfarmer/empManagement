#ifndef LOCKER_HPP
#define LOCKER_HPP

namespace libemployee
    {
    class Locker
    {
    public:
        enum SEX
        {
            FEMALE,
            MALE
        };
    private:
        int intID;
        int num;
        int room;
        SEX sex;
    public:
        Locker(int intID, int num, int room, SEX sex);
        int getintID() const;
        int getNum() const;
        int getRoom() const;
        SEX getSex() const;
    };
}

#endif // LOCKER_HPP
