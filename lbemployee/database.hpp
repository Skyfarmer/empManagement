#ifndef DATABASE_HPP
#define DATABASE_HPP
#include <QSqlDatabase>
#include <QString>
#include <QList>

#include "employee.hpp"
#include "locker.hpp"

namespace libemployee
{
    enum LIBEMP_ERROR
    {
        LIBEMP_UNKNOWN_ERROR,
        LIBEMP_DATABASE_NAME_MISSING,
        LIBEMP_DATABASE_CONNECTION_ERROR,
        LIBEMP_DATABASE_INCOMPATIBLE,
        LIBEMP_DATABASE_EXECUTION_ERROR,
        LIBEMP_EMPLOYEE_INFO_MISSING,
        LIBEMP_EMPLOYEE_LOCKER_NOT_AVAILABLE,
        LIBEMP_EMPLOYEE_LOCKER_NOT_EXISTENT,
        LIBEMP_EMPLOYEE_LOCKER_INCOMPATIBLE,
        LIBEMP_EMPLOYEE_EXISTS,
        LIBEMP_EMPLOYEE_NOT_EXISTENT
    };

    class Database
    {
    private:
        // The connection to the database
        QSqlDatabase db;
        QString databaseName;


        void cleanUp();
        bool isUserValid(const Employee &emp);
        bool isUserExistent(const int userID);
        bool prepareUserMod(const Employee &emp);
    public:
        Database(QSqlDatabase db);
        ~Database();

        //Modifying the database
        void addUser(const Employee &emp);
        void updateUser(const Employee &emp); //<--- includes claiming a locker
        void removeUser(const int userID);
        void releaseLocker(const int userID);

        //Reading data from database
        void fetchLocker(QList<Locker> *list, QString criteria = "");
        void fetchEmployee(QList<Employee> *list, QString criteria = "");
    };
}
#endif // DATABASE_HPP
